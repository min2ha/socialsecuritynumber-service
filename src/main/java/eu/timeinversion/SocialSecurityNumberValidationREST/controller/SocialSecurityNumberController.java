package eu.timeinversion.SocialSecurityNumberValidationREST.controller;


/*
The API may support:
- Create new company
- Get a list of all companies
- Get details about a company
- Update a company
- Add an owner of the company
- Check of Social Security Number


Simulate a backend that can check the Social Security Number by randomly returning "valid/invalid".
The API should define at least two access groups;
e.g. one that will not be able to read the Social Security Number.

You do not need consider the security of authenticating to the web service,
but propose a protocol/method and justify your choice.
* */

import eu.timeinversion.SocialSecurityNumberValidationREST.model.CustomerRequest;
import eu.timeinversion.SocialSecurityNumberValidationREST.model.Owner;
import eu.timeinversion.SocialSecurityNumberValidationREST.repository.OwnerRepository;
import eu.timeinversion.SocialSecurityNumberValidationREST.services.SocialSecurityNumberService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static eu.timeinversion.SocialSecurityNumberValidationREST.config.SwaggerConfig.BASIC_AUTH_SECURITY_SCHEME;

/***
 * Social Security Number Controller
 */
@RestController
@RequestMapping("/api")
public class SocialSecurityNumberController {
    private static final Logger log = LoggerFactory.getLogger(SocialSecurityNumberController.class);
    private final OwnerRepository repository;

    SocialSecurityNumberController(OwnerRepository repository) {
        this.repository = repository;
    }

    @Autowired
    SocialSecurityNumberService ssnService;

    @Operation(summary = "Get owners from public endpoint")
    @GetMapping("/public/owners")
    List<Owner> all() {
        return repository.findAll();
    }


    /*
    *
    curl --header "Content-Type: application/json" --request POST --data '{"socialSecurityNumber":"SNN123"}' -u bgates:123 localhost:8080/api/private/validatessn
    *
    * */

    //validate social security number
    @Operation(
            summary = "Validate SSN from private endpoint",
            security = {@SecurityRequirement(name = BASIC_AUTH_SECURITY_SCHEME)})
    @PostMapping(value = "/private/validatessn")
    public ResponseEntity<Object> ssnValidation(@RequestBody CustomerRequest customerRequest) {
        log.info("CustomerRequest.ts Data: socialSecurityNumber = " + customerRequest.getSocialSecurityNumber() );

        boolean result = repository.findAll().stream().anyMatch(num -> num.getSocialSecurityNumber().equals(customerRequest.getSocialSecurityNumber()));
        
        if (result)
            return new ResponseEntity<>(
                "SSN is valid",
                HttpStatus.OK);
        else
            return new ResponseEntity<>(
                    "SSN is invalid",
                    HttpStatus.OK);
        //return new ResponseEntity<>(ssnService.processCustomerRequest(customerRequest), HttpStatus.OK);
    }
}
