package eu.timeinversion.SocialSecurityNumberValidationREST.controller;

import eu.timeinversion.SocialSecurityNumberValidationREST.exception.CompanyNotFoundException;
import eu.timeinversion.SocialSecurityNumberValidationREST.model.Company;
import eu.timeinversion.SocialSecurityNumberValidationREST.repository.CompanyRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

import static eu.timeinversion.SocialSecurityNumberValidationREST.config.SwaggerConfig.BASIC_AUTH_SECURITY_SCHEME;

/*
HTTP verbs.

POST - Create a new resource
GET - Read a resource
PUT - Update an existing resource
DELETE - Delete a resource
HTTP also defines standard response codes.

200 - SUCCESS
404 - RESOURCE NOT FOUND
400 - BAD REQUEST
201 - CREATED
401 - UNAUTHORIZED
415 - UNSUPPORTED TYPE - Representation not supported for the resource
500 - SERVER ERROR

* */


/**
 * Retrieve all companies - @GetMapping(“/public/companies”)
 * Get details of specific company - @GetMapping(“/public/companies/{id}”)
 * Delete a company - @DeleteMapping(“/public/companies/{id}”)
 * Create a new company - @PostMapping(“/public/companies”)
 * Update company details - @PutMapping(“/public/companies/{id}”)
 */

@RestController
@RequestMapping("/api")
public class CompanyController {
    private final CompanyRepository repository;

    CompanyController(CompanyRepository repository) {
        this.repository = repository;
    }


    /*
    curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"name":"xyz1","country":"xyz2", "phoneNumber":"76547654"}' \
  localhost:8080/api/public/companies
  http://localhost:8080/api/public/companies
    * */
    //Create new company
    @PostMapping("/public/companies")
    public ResponseEntity<Object> createStudent(@RequestBody Company company) {
        Company savedStudent = repository.save(company);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(savedStudent.getId()).toUri();

        return ResponseEntity.created(location).build();
    }


    @Operation(summary = "Get companies from public endpoint")
    @GetMapping("/public/companies")
    List<Company> all() {
        return repository.findAll();
    }

    @Operation(summary = "Get company from public endpoint")
    @GetMapping("/public/companies/{id}")
    public Company retrieveCompany(@PathVariable long id) {
        Optional<Company> companyOptional = repository.findById(id);

        if (!companyOptional.isPresent())
            throw new CompanyNotFoundException("id-" + id);

        return companyOptional.get();
    }

    @Operation(
            summary = "Get string from private/secured endpoint",
            security = {@SecurityRequirement(name = BASIC_AUTH_SECURITY_SCHEME)})
    @GetMapping("/private/companyowner")
    public String getPrivateString(Principal principal) {
        return principal.getName() + ", it is private.\n";
    }

}
