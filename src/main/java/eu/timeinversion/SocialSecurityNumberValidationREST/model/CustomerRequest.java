package eu.timeinversion.SocialSecurityNumberValidationREST.model;

public class CustomerRequest {

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    private String socialSecurityNumber;
}
