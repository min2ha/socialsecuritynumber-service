package eu.timeinversion.SocialSecurityNumberValidationREST.database;

import eu.timeinversion.SocialSecurityNumberValidationREST.model.Company;
import eu.timeinversion.SocialSecurityNumberValidationREST.model.Owner;
import eu.timeinversion.SocialSecurityNumberValidationREST.repository.CompanyRepository;
import eu.timeinversion.SocialSecurityNumberValidationREST.repository.OwnerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoadDatabase {
    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    CommandLineRunner initDatabase(CompanyRepository repository) {
        return args -> {
            log.info("Preloading " + repository.save(new Company("Bilbo Baggins", "United Kingdom", "65437654")));
            log.info("Preloading " + repository.save(new Company("Frodo Baggins", "United Kingdom", "65437657")));

        };
    }

    @Bean
    CommandLineRunner initDatabase2(OwnerRepository repository) {
        return args -> {
            log.info("Preloading " + repository.save(new Owner("Frodo Baggins", "SSN654")));
            log.info("Preloading " + repository.save(new Owner("Bilbo Baggins", "SSN657")));
            log.info("Preloading " + repository.save(new Owner("Bill Gates", "SSN777")));
        };
    }
}
