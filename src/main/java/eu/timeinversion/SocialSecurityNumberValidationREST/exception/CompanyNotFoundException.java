package eu.timeinversion.SocialSecurityNumberValidationREST.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND)
public class CompanyNotFoundException extends RuntimeException{

    private String id;

    public CompanyNotFoundException(String id) {
        super(String.format(" not found : '%s'",id));
        this.id=id;

    }

    public String getId() {
        return this.id;
    }

}

