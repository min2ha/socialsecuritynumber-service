package eu.timeinversion.SocialSecurityNumberValidationREST.services;

import eu.timeinversion.SocialSecurityNumberValidationREST.model.CustomerRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SocialSecurityNumberServiceImpl implements SocialSecurityNumberService {
    private static final Logger log = LoggerFactory.getLogger(SocialSecurityNumberServiceImpl.class);

    /***
     * Backend main method (Business Logic)
     * @param customerRequest
     * @return
     *
     * (Based on JVM 8 Lambda functions,
     * more recent versions of JVM could suggest more optimal code)
     * */
    @Override
    public List<String> processCustomerRequest(CustomerRequest customerRequest) {

        log.debug("CustomerRequest.ts Data: socialSecurityNumber = " + customerRequest.getSocialSecurityNumber() );
        //business logic
        /*
        return productRepo.values()
                .stream()
                .filter(x -> x.getSocialSecurityNumber() == customerRequest.getSocialSecurityNumber())
                .map(x -> x.getName())
                .collect(Collectors.toList());

                */
        return null;
    }
}
