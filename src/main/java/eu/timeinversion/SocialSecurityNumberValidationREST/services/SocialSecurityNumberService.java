package eu.timeinversion.SocialSecurityNumberValidationREST.services;

import eu.timeinversion.SocialSecurityNumberValidationREST.model.CustomerRequest;

import java.util.List;

public interface SocialSecurityNumberService {
    public abstract List<String> processCustomerRequest(CustomerRequest customerRequest);
}
