package eu.timeinversion.SocialSecurityNumberValidationREST.repository;

import eu.timeinversion.SocialSecurityNumberValidationREST.model.Owner;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OwnerRepository extends JpaRepository<Owner, Long> {
}
