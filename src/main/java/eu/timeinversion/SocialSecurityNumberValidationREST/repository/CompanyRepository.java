package eu.timeinversion.SocialSecurityNumberValidationREST.repository;

import eu.timeinversion.SocialSecurityNumberValidationREST.model.Company;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyRepository extends JpaRepository<Company, Long> {
}
