# Social security number service

The goal of this project is to create a simple [`Spring Boot`](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/) REST API, called `SocialSecurityNumber-service`, and secure it with `Spring Security LDAP` module.

__*(Sorry about the text formatting, due to old Markdown on BitBucket it was too much time consuming compared to GitHub so I left as it is)*__

## Application

- ### SocialSecurityNumber-service

  `Spring Boot` Java Web application that exposes two endpoints:
   - `GET /api/public/companies`: that can be access by anyone, it is not secured;
   - `GET /api/private/companyowner`: that can just be accessed by users authenticated with valid LDAP credentials.

## Prerequisites

- [`Java 11+`](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)
- [`Docker`](https://www.docker.com/)
- [`Docker-Compose`](https://docs.docker.com/compose/install/)
- `Chrome browser` (recommended for Swagger)

## Start Environment
Clone GIT repository
```
git clone https://min2ha@bitbucket.org/min2ha/socialsecuritynumber-service.git
```

Open a terminal and inside `SocialSecurityNumber-service` root folder run
```
docker-compose up -d
```

Check their status by running
```
docker-compose ps
```

## Import OpenLDAP Users

The `LDIF` file we will use, `SocialSecurityNumber-service/ldap/ldap-mycompany-com.ldif`, contains a pre-defined structure for `mycompany.com`. Basically, it has 2 groups (`employees` and `clients`) and 3 users (`Bill Gates`, `Steve Jobs` and `Mark Cuban`). Besides, it's defined that `Bill Gates` and `Steve Jobs` belong to `employees` group and `Mark Cuban` belongs to `clients` group.
```
Bill Gates > username: bgates, password: 123
Steve Jobs > username: sjobs, password: 123
Mark Cuban > username: mcuban, password: 123
```

### Import users running a script

In a terminal, make use you are in `SocialSecurityNumber-service` root folder

Run the following script
```
./import-openldap-users.sh
```
  
Check users imported using [`ldapsearch`](https://linux.die.net/man/1/ldapsearch)
```
ldapsearch -x -D "cn=admin,dc=mycompany,dc=com" \
  -w admin -H ldap://localhost:389 \
  -b "ou=users,dc=mycompany,dc=com" \
  -s sub "(uid=*)"
```

Administrator credentials
```
Login DN: cn=admin,dc=mycompany,dc=com
Password: admin
```

## Run application

In a terminal, make use you are in `SocialSecurityNumber-service` root folder

Run the following command to start `SocialSecurityNumber-service`
```
./mvnw clean spring-boot:run
```

## Testing using curl

1.  Open a terminal

2.  Call the endpoint `/api/public/companies`
    ```
    curl -i localhost:8080/api/public/companies
    ```

    It should return
    ```
    HTTP/1.1 200
    It is public.
    ```

3.  Insert new record to Company data table, call the endpoint `/api/public/companies`
    ```
    curl --header "Content-Type: application/json"
    --request POST 
    --data '{"name":"NewCompany","country":"Australia", "phoneNumber":"9988998899"}' 
    -u bgates:123 
    localhost:8080/api/public/companies
    ```

    It should return
   ```
    HTTP/1.1 200
    It is public.
    ```

4.  Try to call the endpoint `/api/private/companyowner` without credentials
    ``` 
    curl -i localhost:8080/api/private/companyowner
    ```
   
    It should return
    ```
    HTTP/1.1 401
    {
     "timestamp": "2018-06-02T22:39:18.534+0000",
     "status": 401,
     "error": "Unauthorized",
     "message": "Unauthorized",
     "path": "/api/private/companyowner"
    }
    ```

5.  Call the endpoint `/api/private/companyowner` again. This time informing `username` and `password`
    :`curl -i -u bgates:123 localhost:8080/api/private/companyowner`
    It should return
    ```
    HTTP/1.1 200
    bgates, it is private.
    ```

6.  Call the endpoint `/api/private/companyowner` informing an invalid password
    ``` 
    curl -i -u bgates:124 localhost:8080/api/private/companyowner
    ```
   
    It should return
    ```
    HTTP/1.1 401 
    ```

7.  Call the endpoint `/api/private/validatessn` informing about SSN validation
    ``` 
    curl --header "Content-Type: applicationst POST --data '{"socialSecurityNumber":"SNN777"}' -u bgates:123 localhost:8080/api/private/validatessn
    ```
   
    It should return
    ```
    SSN is invalid
    ```
    or 
    ```
    SSN is valid
    ```

## Testing using Swagger

1. Access http://localhost:8080/swagger-ui.html

   ![swagger](images/swagger.png)

1. Click on `GET /api/public/companies`, then on `Try it out` button and, finally, on `Execute` button.

   It should return
   ```
   Code: 200
   Response Body: It is public.
   ```

1. Click on `Authorize` button (green one, almost on the top of the page, on the right)

1. In the form that opens, provide the `Bill Gates` credentials, i.e, username `bgates` and password `123`. Then, click on `Authorize` and finally on `Close`

1. Click on `GET /api/private/companyowner`, then click on `Try it out` button and, finally, on `Execute` button.

   It should return
   ```
   Code: 200
   Response Body: bgates, it is private.
   ```

## Shutdown

- Go to the terminal where `SocialSecurityNumber-service` is running and press `Ctrl+C`

- In `SocialSecurityNumber-service` root folder, to stop and remove docker-compose containers, networks and volumes run
  ```
  docker-compose down -v
  ```
